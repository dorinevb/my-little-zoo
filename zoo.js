const animalListElement = document.getElementById("animal-list");

const animalEmojiElement = document.getElementById("animal-emoji");

const animalNameElement = document.getElementById("animal-name");

const createAnimalButton = document.getElementById("create-animal");

createAnimalButton.addEventListener("click", addAnimal);

animals = [];

function addAnimal() {
  const newAnimalName = animalNameElement.value.trim();
  const newAnimalEmoji = animalEmojiElement.value.trim();
  const newId = generateId();
  const newAnimal = new Animal(newId, newAnimalName, newAnimalEmoji);
  animals.push(newAnimal);
  renderAnimals();
}

function generateId() {
  const id = Math.random().toString(16).slice(2);
  return id;
}

function Animal(id, name, emoji) {
  this.id = id
  this.name = name;
  this.emoji = emoji;
}

function renderAnimals() {
  animalListElement.innerHTML = "";
  for (const animal of animals) {
    const liElement = document.createElement("li");
    liElement.innerText = animal.name + ": " + animal.emoji + " id: " + animal.id;
    animalListElement.appendChild(liElement);
  }
}